package com.fercugliandro.mastertech.auditfinanproducer.service;

import com.fercugliandro.mastertech.auditfinanproducer.model.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class AuditoriaFinanceiraProducerService {

    @Autowired
    private KafkaTemplate<String, String> sender;

    public void enviarEmpresa(String cnpj) {
        sender.send("luis-biro-1", "1", cnpj);
    }
}