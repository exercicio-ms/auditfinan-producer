package com.fercugliandro.mastertech.auditfinanproducer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuditfinanProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuditfinanProducerApplication.class, args);
	}

}
