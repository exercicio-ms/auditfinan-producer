package com.fercugliandro.mastertech.auditfinanproducer.controller;

import com.fercugliandro.mastertech.auditfinanproducer.service.AuditoriaFinanceiraProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuditoriaFinanceiraController {

    @Autowired
    private AuditoriaFinanceiraProducerService service;

    @GetMapping("/auditoria/{cnpj}")
    public void enviarEmpresa(@PathVariable String cnpj) {
        service.enviarEmpresa(cnpj);
    }
}
